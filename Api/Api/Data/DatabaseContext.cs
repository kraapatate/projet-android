﻿using Api.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Data
{
    public class DatabaseContext : DbContext
    {

        public DatabaseContext(DbContextOptions<DatabaseContext> opt) : base(opt) { }

        public DbSet<Statut> Statut { get; set; }
        public DbSet<Bilan> Bilan { get; set; }

        public DbSet<Classe> Classe { get; set; }
        public DbSet<Entreprise> Entreprise { get; set; }
        public DbSet<Etudiant> Etudiant { get; set; }
        public DbSet<NumeroBilan> NumeroBilan { get; set; }
        public DbSet<Parametre> Parametre { get; set; }

        public DbSet<Profil> Profil { get; set; }
        public DbSet<Specialiste> Specialiste { get; set; }
        public DbSet<Tuteur> Tuteur { get; set; }
       











    }
}
