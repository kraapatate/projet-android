﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.Data;
using Api.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class StatutController : ControllerBase
    {

        private readonly ILogger<StatutController> _logger;
        private readonly DatabaseContext context;

        public StatutController(ILogger<StatutController> logger, DatabaseContext context)
        {
            _logger = logger;
            this.context = context;
        }

        [HttpGet]
        public IEnumerable<Statut> Get()
        {
            return context.Statut;
        }

    }
}
