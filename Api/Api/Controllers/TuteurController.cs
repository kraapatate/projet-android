﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.Data;
using Api.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TuteurController : ControllerBase
    {

        private readonly ILogger<TuteurController> _logger;
        private readonly DatabaseContext context;

        public TuteurController(ILogger<TuteurController> logger, DatabaseContext context)
        {
            _logger = logger;
            this.context = context;
        }

        [HttpGet]
        public IEnumerable<Tuteur> Get()
        {
            return context.Tuteur;
        }

    }
}
