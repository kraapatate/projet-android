﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.Data;
using Api.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BilanController : ControllerBase
    {

        private readonly ILogger<BilanController> _logger;
        private readonly DatabaseContext context;

        public BilanController(ILogger<BilanController> logger, DatabaseContext context)
        {
            _logger = logger;
            this.context = context;
        }

        [HttpGet]
        public IEnumerable<Bilan> Get()
        {
            return context.Bilan;
        }
    }
}
