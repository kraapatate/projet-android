﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.Data;
using Api.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EntrepriseController : ControllerBase
    {

        private readonly ILogger<EntrepriseController> _logger;
        private readonly DatabaseContext context;

        public EntrepriseController(ILogger<EntrepriseController> logger, DatabaseContext context)
        {
            _logger = logger;
            this.context = context;
        }

        [HttpGet]
        public IEnumerable<Entreprise> Get()
        {
            return context.Entreprise;
        }

    }
}
