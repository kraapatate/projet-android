﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.Data;
using Api.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProfilController : ControllerBase
    {

        private readonly ILogger<ProfilController> _logger;
        private readonly DatabaseContext context;

        public ProfilController(ILogger<ProfilController> logger, DatabaseContext context)
        {
            _logger = logger;
            this.context = context;
        }

        [HttpGet]
        public IEnumerable<Profil> Get()
        {
            return context.Profil;
        }

    }
}
