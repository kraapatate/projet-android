﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.Data;
using Api.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SpecialisteController : ControllerBase
    {

        private readonly ILogger<SpecialisteController> _logger;
        private readonly DatabaseContext context;

        public SpecialisteController(ILogger<SpecialisteController> logger, DatabaseContext context)
        {
            _logger = logger;
            this.context = context;
        }

        [HttpGet]
        public IEnumerable<Specialiste> Get()
        {
            return context.Specialiste;
        }

    }
}
    