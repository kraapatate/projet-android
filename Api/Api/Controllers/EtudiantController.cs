﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.Data;
using Api.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EtudiantController : ControllerBase
    {

        private readonly ILogger<EtudiantController> _logger;
        private readonly DatabaseContext context;

        public EtudiantController(ILogger<EtudiantController> logger, DatabaseContext context)
        {
            _logger = logger;
            this.context = context;
        }

        [HttpGet]
        public IEnumerable<Etudiant> Get()
        {
            return context.Etudiant;
        }

    }
}
