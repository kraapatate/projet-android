﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.Data;
using Api.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class NumeroBilanController : ControllerBase
    {

        private readonly ILogger<NumeroBilanController> _logger;
        private readonly DatabaseContext context;

        public NumeroBilanController(ILogger<NumeroBilanController> logger, DatabaseContext context)
        {
            _logger = logger;
            this.context = context;
        }

        [HttpGet]
        public IEnumerable<NumeroBilan> Get()
        {
            return context.NumeroBilan;
        }

    }
}
