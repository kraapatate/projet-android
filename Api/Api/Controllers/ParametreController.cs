﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.Data;
using Api.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ParametreController : ControllerBase
    {

        private readonly ILogger<ParametreController> _logger;
        private readonly DatabaseContext context;

        public ParametreController(ILogger<ParametreController> logger, DatabaseContext context)
        {
            _logger = logger;
            this.context = context;
        }

        [HttpGet]
        public IEnumerable<Parametre> Get()
        {
            return context.Parametre;
        }

    }
}
