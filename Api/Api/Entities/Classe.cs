﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Entities
{
    public class Classe
    {
        [Key]
        [Column("Id_Cla")]
        public int Id { get; set; }
        [Column("Lib_Cla")]
        public string Lib { get; set; }
    }
}
