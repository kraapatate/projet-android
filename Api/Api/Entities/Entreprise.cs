﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Entities
{
    public class Entreprise
    {
        [Key]
        [Column("id_Ent")]
        public int IdEnt { get; set; }

        [Column("Lib_Ent")]
        public string LibEnt { get; set; }

        [Column("Adr_Ent")]
        public string AdrEnt { get; set; }

        [Column("Cp_Ent")]
        public string CpEnt { get; set; }

        [Column("Vil_Ent")]
        public string VilEnt { get; set; }
    }
}
