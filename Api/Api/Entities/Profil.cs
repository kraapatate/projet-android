﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Entities
{
    public class Profil
    {
        [Key]
        [Column("id_Pro")]
        public int Id { get; set; }

        [Column("Lib_Pro")]
        public string Lib { get;set; }
    }
}
