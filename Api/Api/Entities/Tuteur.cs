﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Entities
{
    public class Tuteur
    {
        [Key]
        [Column("Num_Tut")]
        public int Id { get; private set; }

        [Column("Nom_Tut")]
        public string Nom { get; private set; }

        [Column("Pre_Tut")]
        public string Prenom { get; private set; }

        [Column("Tel_Tut")]
        public string Telephone { get; private set; }

        [Column("Mai_Tut")]
        public string Mail { get; private set; }

        [Column("Log_Tut")]
        public string Login { get; private set; }

        [Column("Mdp_Tut")]
        public string MotDePasse { get; private set; }

        [Column("Cpt_Val")]
        public uint? Cpt { get; private set; }

        [Column("Nb_Etu_Max_3OLE")]
        public int NombreEtudiantsMax3Olen { get; private set; }

        [Column("Nb_Etu_Max_4OLE")]
        public int NombreEtudiantsMax4Olen { get; private set; }

        [Column("Nb_Etu_Max_5OLE")]
        public int NombreEtudiantsMax5Olen { get; private set; }

        [Column("id_Pro")]
        [ForeignKey("id_Pro")]
        public int IdPro { get; private set; }
    }
}
