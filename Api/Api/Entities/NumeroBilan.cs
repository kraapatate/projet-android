﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Entities
{
    public class NumeroBilan
    {
        [Key]
        [Column("Num_Bil")]
        public int NumBil { get; set; }
        [Column("Lib_Bil")]
        public string LibBil { get; set; }

    }
}
