﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Entities
{
    public class Specialiste
    {
        [Key]
        [Column("Id_Spe")]
        public int Id { get; set; }

        [Column("Lib_Spe")]
        public string Lib { get; set; }



    }
}
