﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Entities
{
    public class Bilan { 

    
        [Key]
        [Column("Id_Bil")]
        public int Id { get; set; }

        [Column("Dat_Bil")]
        public DateTime Dat { get; set; }

        [Column("Not_Ent_Bil")]
        public float NotEnt { get; set; }

        [Column("Not_Ora_Bil")]
        public float NotOra { get; set; }

        [Column("Not_Dos_Bil")]
        public float NotDos { get; set; }

        [Column("Note_Ora_Fin_Bil")]
        public float NoteOraFin { get; set; }

        [Column("Note_Dos_Fin_Bil")]
        public float NoteDosFin { get; set; }

        [Column("Rem_Bil")]
        public string RemBil { get; set; }

        [ForeignKey("Num_Bil")]
        [Column("Num_Bil")]
        public int NumBil { get; set; }

        [ForeignKey("Num_Etu")]
        [Column("Num_Etu")]
        public int NumEtu { get; set; }





    }
}
