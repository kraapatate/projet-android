﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Entities
{
    public class Etudiant
    {
        [Key]
        [Column("Num_Etu")]
        public int NumEtu { get; set; }

        [Column("Nom_Etu")]
        public string NomEtu { get; set; }

        [Column("Pre_Etu")]
        public string PreEtu { get; set; }

        [Column("Tel_Etu")]
        public string TelEtu { get; set; }

        [Column("Mai_Etu")]
        public string MaiEtu { get; set; }

        [Column("Log_Etu")]
        public string LogEtu { get; set; }

        [Column("Mdp_Etu")]
        public string MdpEtu { get; set; }

        [Column("Rem_Etu")]
        public string RemEtu { get; set; }

        [Column("Dat_Pnt_Tel_Etu")]
        public DateTime DatPntTelEtu { get; set; }

        [Column("Suj_Etu")]
        public string SujEtu { get; set; }
        

        [ForeignKey("Id_Cla")]
        [Column("Id_Cla")]
        public int IdCla { get; set; }
        [ForeignKey("Id_Spe")]
        [Column("Id_Spe")]
        public int IdSpe { get; set; }

        [ForeignKey("Num_Etu")]
        [Column("Num_Etu")]
        public int NumTut { get; set; }

        [ForeignKey("Id_Sta")]
        [Column("Id_Sta")]
        public int IdSta { get; set; }

        [ForeignKey("id_Ent")]
        [Column("id_Ent")]
        public int idEnt { get; set; }



    }
}
