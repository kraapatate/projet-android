﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Entities
{
    public class Statut
    {
        [Key]
        [Column("Id_Sta")]
        public int Id { get; set; }

        [Column("Lib_Sta")]
        public string Lib { get; set; }

        
    }
    
}
