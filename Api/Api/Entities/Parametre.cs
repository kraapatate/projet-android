﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Entities
{
    public class Parametre {

        [Column("App_Tel_3_OLE")]
        public DateTime AppTel3Olen { get; set; }

        [Column("Vis_Ent_3_OLE")]
        public DateTime VisEnt3Olen { get; set; }

        [Column("App_Tel__Sta_3_OLE")]
        public DateTime AppTelSta3Olen { get; set; }

        [Column("Vis_Ent_Sta_3_OLE")]
        public DateTime VisEntSta3Olen { get; set; }

        [Column("Bil_2_3_OLE")]
        public DateTime Bil23Olen { get; set; }

        [Column("Ent_Aou_3_OLE")]
        public DateTime EntAou3Olen { get; set; }

        [Column("App_Tel_4_OLE")]
        public DateTime AppTel4Olen { get; set; }

        [Column("Bil_1_4_OLE")]
        public DateTime Bil14Olen { get; set; }

        [Column("Poi_Mem_4_OLE")]
        public DateTime PoiMem4Olen { get; set; }

        [Column("Bil_2_4_OLE")]
        public DateTime Bil24Olen { get; set; }

        [Column("Ent_Mai_4_OLE")]
        public DateTime EntMai4Olen { get; set; }

        [Column("App_Tel_5_OLE")]
        public DateTime AppTel5Olen { get; set; }

        [Column("Vis_Ent_5_OLE")]
        public DateTime VisEnt5Olen { get; set; }

        [Column("Ent_Mar_5_OLE")]
        public DateTime EntMar5Olen { get; set; }

        [Column("Bil_2_5_OLE")]
        public DateTime Bil25Olen { get; set; }

        [Column("Ent_Jui_5_OLE")]
        public DateTime EntJui5Olen { get; set; }

        [Column("Ent_Aou_5_OLE")]
        public DateTime EntAou5Olen { get; set; }
        









    }
}
